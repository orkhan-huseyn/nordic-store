import { Link } from 'react-router-dom';
import { AccountIcon, CartIcon, MenuIcon, NordicsIcon } from '../icons';
import Navigation from './Navigation';

function Header() {
  return (
    <header id="header" className="w-full z-30 top-0 py-1">
      <div className="w-full container mx-auto flex flex-wrap items-center justify-between mt-0 px-6 py-3">
        <label htmlFor="menu-toggle" className="cursor-pointer md:hidden block">
          <MenuIcon />
        </label>
        <input className="hidden" type="checkbox" id="menu-toggle" />
        <div
          id="menu"
          className="hidden md:flex md:items-center md:w-auto w-full order-3 md:order-1"
        >
          <Navigation />
        </div>

        <div className="order-1 md:order-2">
          <Link
            to="/"
            className="flex items-center tracking-wide no-underline hover:no-underline font-bold text-gray-800 text-xl"
          >
            <NordicsIcon />
            NORDICS
          </Link>
        </div>

        <div id="nav-content" className="order-2 md:order-3 flex items-center">
          <a className="inline-block no-underline hover:text-black" href="#">
            <AccountIcon />
          </a>
          <a
            className="pl-3 inline-block no-underline hover:text-black"
            href="#"
          >
            <CartIcon />
          </a>
        </div>
      </div>
    </header>
  );
}

export default Header;
