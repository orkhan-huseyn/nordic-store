import { Link } from 'react-router-dom';

function Navigation() {
  return (
    <nav>
      <ul className="md:flex items-center justify-between text-base text-gray-700 pt-4 md:pt-0">
        <li>
          <Link
            to="/shop"
            className="inline-block no-underline hover:text-black hover:underline py-2 px-4"
          >
            Shop
          </Link>
        </li>
        <li>
          <Link
            to="/about"
            className="inline-block no-underline hover:text-black hover:underline py-2 px-4"
          >
            About
          </Link>
        </li>
      </ul>
    </nav>
  );
}

export default Navigation;
