import { fireEvent, render, screen } from '@testing-library/react';
import { axe } from 'jest-axe';
import ProductFilters from '../ProductFilters';

describe('ProductFilters', () => {
  it('should render', () => {
    render(<ProductFilters onSearch={jest.fn()} />);
    const searchInput = screen.queryByPlaceholderText('Search product...');
    expect(searchInput).toBeInTheDocument();

    fireEvent.change(searchInput, { target: { value: 'iPhone' } });
    expect(searchInput.value).toBe('iPhone');
  });

  it('should render', async () => {
    const { container } = render(<ProductFilters onSearch={jest.fn()} />);
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });
});
