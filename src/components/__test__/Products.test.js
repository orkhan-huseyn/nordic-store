import { act, render, screen } from '@testing-library/react';
import Products from '../Products';

jest.mock('../../api/fetch', () => {
  return {
    fetchJSON: (url) => {
      return Promise.resolve([]);
    },
  };
});

describe('Products', () => {
  it('should show product list', () => {
    render(<Products />);
    const loadingText = screen.getByText('Loading...');
    expect(loadingText).toBeInTheDocument();
  });
});
