import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import ProductItem from '../ProductItem';

describe('ProductItem', () => {
  it('should show single product with given props', () => {
    render(
      <BrowserRouter>
        <ProductItem id="test" name="test" image="test" price="99" />
      </BrowserRouter>
    );
    const productTitle = screen.queryByText('test');
    const priceSection = screen.queryByText('£99');
    const productImage = screen.queryByAltText('test');
    expect(productTitle).toBeInTheDocument();
    expect(priceSection).toBeInTheDocument();
    expect(productImage).toBeInTheDocument();
    expect(productImage).toBeVisible();
  });
});
