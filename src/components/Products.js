import { useState } from 'react';
import { useProducts } from '../hooks/useProducts';
import ProductFilters from './ProductFilters';
import ProductItem from './ProductItem';

function Products() {
  const [keyword, setKeyword] = useState('');
  const [products, total, loading, error, loadMore] = useProducts(keyword);

  function handleSearch(value) {
    setKeyword(value);
  }

  return (
    <section className="bg-white py-8">
      <div className="container mx-auto flex items-center flex-wrap pt-4 pb-12">
        <ProductFilters onSearch={handleSearch} />
        {loading && (
          <span className="w-full text-center text-lg font-bold">
            Loading...
          </span>
        )}
        {error && (
          <span className="w-full text-center text-lg font-bold">
            Oops! Something went wrong...
          </span>
        )}
        {products.map((product) => (
          <ProductItem
            key={product.id}
            id={product.id}
            name={product.title}
            price={product.price}
            image={product.thumbnail}
          />
        ))}
      </div>
      {!loading && !error && products.length < total && (
        <div className="flex items-center justify-center">
          <button onClick={loadMore}>Load more</button>
        </div>
      )}
    </section>
  );
}

export default Products;
