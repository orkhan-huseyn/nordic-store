import { useCallback, useEffect, useState } from 'react';
import debounce from 'lodash/debounce';
import { SearchIcon, SortIcon } from '../icons';

function ProductFilters({ onSearch }) {
  const [search, setSearch] = useState('');

  const debouncedSearch = useCallback(debounce(onSearch, 500), []);

  function handleChange(event) {
    const value = event.target.value;
    setSearch(value);
    debouncedSearch(value);
  }

  return (
    <nav id="store" className="w-full z-30 top-0 px-6 py-1">
      <div className="w-full container mx-auto flex flex-wrap items-center justify-between mt-0 px-2 py-3">
        <a
          aria-label="Go to store"
          className="uppercase tracking-wide no-underline hover:no-underline font-bold text-gray-800 text-xl"
          href="#"
        >
          Store
        </a>

        <div className="flex items-center" id="store-nav-content">
          <a
            aria-label="Sort items"
            className="pl-3 inline-block no-underline hover:text-black"
            href="#"
          >
            <SortIcon />
          </a>

          <a
            aria-label="Search items"
            className="pl-3 inline-block no-underline hover:text-black"
            href="#"
          >
            <SearchIcon />
          </a>
        </div>

        <div className="flex items-center">
          <input
            type="search"
            className="p-2"
            placeholder="Search product..."
            value={search}
            onChange={handleChange}
          />
        </div>
      </div>
    </nav>
  );
}

export default ProductFilters;
