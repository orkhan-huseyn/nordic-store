import { fetchJSON } from './fetch';

const BASE_URL = 'https://dummyjson.com';

export function getProducts({ skip = 0, limit = 8, keyword = '' }) {
  const url = `${BASE_URL}/products/search?skip=${skip}&limit=${limit}&q=${keyword}`;
  return fetchJSON(url);
}

export function getProduct(id) {
  return fetchJSON(`${BASE_URL}/products/${id}`);
}
