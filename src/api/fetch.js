export function fetchJSON(url) {
  return fetch(url).then((response) => {
    if (!response.ok) {
      return Promise.reject(response.json());
    }
    return response.json();
  });
}
