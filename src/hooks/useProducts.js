import { useEffect, useState } from 'react';
import { getProducts } from '../api/products';

const DEFAULT_LIMIT = 8;

export function useProducts(keyword) {
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [products, setProducts] = useState([]);
  const [skip, setSkip] = useState(0);
  const [total, setTotal] = useState(0);

  useEffect(() => {
    if (keyword === '') return;

    setLoading(true);
    getProducts({ skip: 0, limit: 100, keyword })
      .then((json) => {
        setTotal(json.total);
        setProducts(json.products);
      })
      .catch(() => {
        setError(true);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [keyword]);

  useEffect(() => {
    if (keyword !== '') return;
    
    setLoading(true);
    getProducts({ skip, limit: DEFAULT_LIMIT })
      .then((json) => {
        setTotal(json.total);
        setProducts((products) => [...products, ...json.products]);
      })
      .catch(() => {
        setError(true);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [skip, keyword]);

  function loadMore() {
    setSkip(skip + DEFAULT_LIMIT);
  }

  return [products, total, loading, error, loadMore];
}
