import { useEffect, useState } from 'react';
import { getProduct } from '../api/products';

export function useProduct(productId) {
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [product, setProduct] = useState(null);

  useEffect(() => {
    setLoading(true);
    getProduct(productId)
      .then((product) => {
        setProduct(product);
      })
      .catch(() => {
        setError(true);
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  return [product, loading, error];
}
