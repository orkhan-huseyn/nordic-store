import React from 'react';

const HomePage = React.lazy(() => import('./HomePage'));

export default () => (
  <React.Suspense fallback={<h1>Loading HomePage...</h1>}>
    <HomePage />
  </React.Suspense>
);
