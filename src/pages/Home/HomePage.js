import Banner from '../../components/Banner';
import Products from '../../components/Products';

function HomePage() {
  return (
    <>
      <Banner />
      <Products />
    </>
  );
}

export default HomePage;
