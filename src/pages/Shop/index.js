import React from 'react';

const ShopPage = React.lazy(() => import('./ShopPage'));

export default () => (
  <React.Suspense fallback={<h1>Loading ShopPage...</h1>}>
    <ShopPage />
  </React.Suspense>
);
