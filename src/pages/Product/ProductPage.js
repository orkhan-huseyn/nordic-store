import { useParams } from 'react-router-dom';
import { useProduct } from '../../hooks/useProduct';

function ProductPage() {
  const { productId } = useParams();
  const [product, loading, error] = useProduct(productId);

  if (error) {
    return <h1>Ooops! Something went wrong.. </h1>;
  }

  if (loading) {
    return <h1>Loading...</h1>;
  }

  return <h1>{JSON.stringify(product)}</h1>;
}

export default ProductPage;
