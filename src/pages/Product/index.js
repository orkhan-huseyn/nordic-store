import React from 'react';

const ProductPage = React.lazy(() => import('./ProductPage'));

export default () => (
  <React.Suspense fallback={<h1>Loading ProductPage...</h1>}>
    <ProductPage />
  </React.Suspense>
);
