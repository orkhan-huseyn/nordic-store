import { Route, Routes } from 'react-router-dom';
import Header from './components/Header';

import HomePage from './pages/Home';
import ProductPage from './pages/Product';
import ShopPage from './pages/Shop';

function App() {
  return (
    <>
      <Header />
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/shop" element={<ShopPage />} />
        <Route path="/products/:productId" element={<ProductPage />} />
      </Routes>
    </>
  );
}

export default App;
